/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import akka.event.LoggingReceive
import scala.collection.immutable.Queue
import scala.util.Random

object BinaryTreeSet {

  trait CustomActorLogger extends ActorLogging {
    this: Actor ⇒

    def debugMsg(id: Int, elem: Int, text: String) = log.debug("operation: " + id + " --> elem: [" + elem + "]" + text)
  }

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}

class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot
  var newRoot = root

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = LoggingReceive{
    case Insert(client, id, elem) => root ! Insert(client, id, elem)
    case Contains(requester, id, elem) => root ! Contains(requester,id, elem)
    case Remove(requester, id, elem) => root ! Remove(requester,id,elem)
    case GC => {
      newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
    }
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = LoggingReceive{
    case operation:Operation => pendingQueue = pendingQueue :+ operation
    case GC => // ignore another GC request
    case CopyFinished => {
      root = newRoot

      pendingQueue.foreach { // process the pending queue of operations...
        case Insert(client, id, elem) => root ! Insert(client, id, elem)
        case Contains(requester, id, elem) => root ! Contains(requester,id, elem)
        case Remove(requester, id, elem) => root ! Remove(requester,id,elem)
      }
      pendingQueue = Queue.empty[Operation]

      context.unbecome() // get the original behavior back...
    }
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with BinaryTreeSet.CustomActorLogger {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  val found = true
  val notFound = false

  def createLeaf(elem: Int): ActorRef = context.actorOf(BinaryTreeNode.props(elem, initiallyRemoved = false))

  def addRightLeaf(elem: Int) = subtrees = subtrees.updated(Right, createLeaf(elem))

  def addLeftLeaf(elem: Int) = subtrees = subtrees.updated(Left, createLeaf(elem))

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = /*LoggingReceive*/ {
    case Contains(requester, id, elementToFind) if elementToFind == elem => {
        if (removed)
          debugMsg(id, elem , "found, but was removed!")
        else
          debugMsg(id, elem, "found!")

      requester ! ContainsResult(id,!removed) ;
    }

    case Contains(requester,id,elementToFind) if elementToFind > elem =>
      subtrees.get(Right).fold(requester ! ContainsResult(id, notFound)) (_ ! Contains(requester,id,elementToFind))

    case Contains(requester,id,elementToFind) if elementToFind < elem =>
      subtrees.get(Left).fold(requester ! ContainsResult(id, notFound)) (_ ! Contains(requester,id,elementToFind))


    case Insert(requester, id, elementToInsert) if elementToInsert == elem => {
      removed = false
      requester ! OperationFinished(id)
    }

    case Insert(requester, id, elementToInsert) if elementToInsert > elem =>
      subtrees.get(Right).fold({
        debugMsg(id, elementToInsert, " inserted to the right of: " + elem)

        addRightLeaf(elementToInsert)
        requester ! OperationFinished(id)}) (_ ! Insert(requester, id, elementToInsert))

    case Insert(requester, id, elementToInsert) if elementToInsert < elem =>
      subtrees.get(Left).fold({
        debugMsg(id, elementToInsert, " inserted to the left of: " + elem)

        addLeftLeaf(elementToInsert)
        requester ! OperationFinished(id)}) (_ ! Insert(requester, id, elementToInsert))


      case Remove(requester, id, elementToRemove) if elementToRemove == elem => {
      debugMsg (id, elem, " removed... " )
      removed = true
      requester ! OperationFinished(id)
    }

    case Remove(requester, id, elementToRemove) if elementToRemove > elem =>
      subtrees.get(Right).fold({requester ! OperationFinished(id) } ) ( { _ ! Remove(requester, id, elementToRemove) })

    case Remove(requester, id, elementToRemove) if elementToRemove < elem =>
      subtrees.get(Left).fold({requester ! OperationFinished(id) }) ({ _ ! Remove(requester, id, elementToRemove) } )


    case CopyTo(newRoot) if !removed => {
      val id = Random.nextInt()

      context.become(copyingMyself(id, newRoot))
      newRoot ! Insert(self, id, elem)
      //subtrees.values.foreach (_ ! CopyTo (newRoot))
    }

    case CopyTo(newRoot) if removed => {
      if(subtrees.isEmpty) {
        stop()
      } else {
        context.become(copying(subtrees.values.toSet, true)) // no need to wait for me, I've gotta tell you good bye kids!
        subtrees.values.foreach(_ ! CopyTo(newRoot))
      }
    }
  }

  def copyingMyself(id: Int, newRoot: ActorRef):Receive = {
    case OperationFinished(id) => {

      if(subtrees.isEmpty) {
        stop()
      } else {
        context.become(copying(subtrees.values.toSet, true))
        subtrees.values.foreach(_ ! CopyTo(newRoot))
      }
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    //when all the expected return OperationFinished, then we should unbecome back to the original logic

   /* case OperationFinished => {
      println("receiving operation finished....")
      self ! CopyFinished;
      context.become(copying(expected, true)) // this is the result of the insertion of myself into the new tree
    }*/

    case CopyFinished => {
      println("copy finished received ")

      if((expected - sender).isEmpty && insertConfirmed) {
        println("making the first copy ")
        stop()
      }
      else {
        println("making the second copy ")
        context.become(copying(expected - sender, insertConfirmed))
      }
    }
  }

  def stop() = {
    context.parent ! CopyFinished
    //context.stop(self)
    self ! PoisonPill
  }


}
