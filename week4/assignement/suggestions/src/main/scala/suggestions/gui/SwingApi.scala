package suggestions
package gui

import scala.language.reflectiveCalls
import scala.concurrent.ExecutionContext.Implicits.global
import scala.swing.Reactions.Reaction
import scala.swing.event.Event
import rx.lang.scala.Observable
import rx.lang.scala.Subscription
import scala.language.postfixOps

/** Basic facilities for dealing with Swing-like components.
*
* Instead of committing to a particular widget implementation
* functionality has been factored out here to deal only with
* abstract types like `ValueChanged` or `TextField`.
* Extractors for abstract events like `ValueChanged` have also
* been factored out into corresponding abstract `val`s.
*/
trait SwingApi {

  type ValueChanged <: Event

  val ValueChanged: {
    def unapply(x: Event): Option[TextField]
  }

  type ButtonClicked <: Event   //ButtonClicked type can refers to any subtype of Event.

  val ButtonClicked: {
    def unapply(x: Event): Option[Button]
  }

  type TextField <: {       // means that classes satisfying this type have to satisfy the following contract.
    def text: String
    def subscribe(r: Reaction): Unit
    def unsubscribe(r: Reaction): Unit
  }

  type Button <: {
    def subscribe(r: Reaction): Unit
    def unsubscribe(r: Reaction): Unit
  }

  implicit class TextFieldOps(field: TextField) {

    /** Returns a stream of text field values entered in the given text field.
      *
      * //@param field the text field
      * @return an observable with a stream of text field updates
      */
    def textValues: Observable[String] = {
      val subscription = Subscription{println("Bye Bye subscription !")}

      Observable.create(observer => {
        field.subscribe {
          case event: ValueChanged => {
            if(subscription.isUnsubscribed) {
              field.unsubscribe {
                case ValueChanged => observer.onNext(field.text)
              }
              println("UNSUBSCRIBED!")
              observer.onCompleted()
            }
            else {
              observer.onNext(field.text)
              println("STILL SUBSCRIBED!")
            }
          }
          case any => println(any) //observer.onError(new IllegalArgumentException)
        }
        subscription
      })
    }

    def textValues2: Observable[String] = {
      //val subscription = Subscription{println("Bye Bye!")}

      Observable(subscriber => {
          field.subscribe {
            case event: ValueChanged => {
              if(subscriber.isUnsubscribed) {
                field.unsubscribe {
                  case ValueChanged => subscriber.onNext(field.text)
                } //those 3 lines have no effect as this reaction doesn't get unsubscribed for some reason!
                println("UNSUBSCRIBED!")
                subscriber.onCompleted()
              }
              else {
                subscriber.onNext(field.text)
                println("STILL SUBSCRIBED!")
              }
          }
          case _ => subscriber.onError(new IllegalArgumentException)
        }
      })
    }

  }

  implicit class ButtonOps(button: Button) {

    /** Returns a stream of button clicks.
     *
     * //@param field the button
     * @return an observable with a stream of buttons that have been clicked
     */
    def clicks: Observable[Button] = {
      val subscription = Subscription{println("button unregistered !")}

      Observable.create(observer => {
        button.subscribe {
          case click: ButtonClicked =>
            if (subscription.isUnsubscribed) {
              button.unsubscribe {
                case click: ButtonClicked => observer.onNext(button)
              }
              println("UNSUBSCRIBED!")
              observer.onCompleted()
            } else {
                observer.onNext(button)
            }
        }
        subscription
      })
    }
  }

}