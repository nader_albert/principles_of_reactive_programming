package suggestions



import scala.collection._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Try, Success, Failure}
import scala.swing.event.Event
import scala.swing.Reactions.Reaction
import rx.lang.scala._
import org.scalatest._
import gui._

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import scala.concurrent.duration._


@RunWith(classOf[JUnitRunner])
class SwingApiTest extends FunSuite {

  object swingApi extends SwingApi {
    class ValueChanged(val textField: TextField) extends Event

    object ValueChanged {
      def unapply(x: Event) = x match {
        case vc: ValueChanged => Some(vc.textField)
        case _ => None
      }
    }

    class ButtonClicked(val source: Button) extends Event

    object ButtonClicked {
      def unapply(x: Event) = x match {
        case bc: ButtonClicked => Some(bc.source)
        case _ => None
      }
    }

    class Component {
      private val subscriptions = mutable.Set[Reaction]()
      def subscribe(r: Reaction) {
        subscriptions add r
      }
      def unsubscribe(r: Reaction) {
        subscriptions remove r
      }
      def publish(e: Event) {
        for (r <- subscriptions) r(e)
      }
    }

    class TextField extends Component {
      private var _text = ""
      def text = _text
      def text_=(t: String) {
        _text = t
        publish(new ValueChanged(this))
      }
    }

    class Button extends Component {
      def click() {
        publish(new ButtonClicked(this))
      }
    }
  }

  import swingApi._
  
  test("emit text field values to the observable with create") {
    val textField = new swingApi.TextField

    val values = textField.textValues

    val observed = mutable.Buffer[String]()

    /*val sub = values subscribe {
      observed += _
    }*/

    val streamSubscription = values subscribe ((s: String) => observed += s, (t: Throwable) => println("Error!" + t), () => println ("Bye! Bye! "))

    // write some text now
    textField.text = "T"
    textField.text = "Tu"
    textField.text = "Tur"
    textField.text = "Turi"
    textField.text = "Turin"
    textField.text = "Turing"

    assert(observed == Seq("T", "Tu", "Tur", "Turi", "Turin", "Turing"), observed)

    streamSubscription.unsubscribe()

    textField.text = "N"
    textField.text = "A"
    textField.text = "D"
    textField.text = "E"
    textField.text = "R"

    assert(observed == Seq("T", "Tu", "Tur", "Turi", "Turin", "Turing"), observed)
  }

  test("emit text field values to the observable with apply") {
    val textField = new swingApi.TextField

    val textStream = textField.textValues2

    val observed = mutable.Buffer[String]()

    /*val sub = values subscribe {
      observed += _
    }*/

    val streamSubscription = textStream subscribe ((s: String) => observed += s, (t: Throwable) => println("Error!" + t), () => println ("Bye!"))

    textField.text = "T"
    textField.text = "Tu"
    textField.text = "Tur"
    textField.text = "Turi"
    textField.text = "Turin"
    textField.text = "Turing"

    assert(observed == Seq("T", "Tu", "Tur", "Turi", "Turin", "Turing"), observed)

    streamSubscription.unsubscribe()

    textField.text = "N"
    textField.text = "A"
    textField.text = "D"
    textField.text = "E"
    textField.text = "R"

    assert(observed == Seq("T", "Tu", "Tur", "Turi", "Turin", "Turing"), observed)
  }

  test("emit button click events to the observable") {

    val button = new swingApi.Button

    val clickStream = button.clicks

    var numberOfClicks = 0

    val clicksSubscription = clickStream.subscribe((s: Button) => { println("button clicked! " + s.toString); numberOfClicks+=1 },
      (t: Throwable) => println("Error!" + t), () => println ("Bye!"))

    button.click()
    button.click()
    button.click()
    button.click()
    button.click()

    assert(numberOfClicks === 5)

    clicksSubscription.unsubscribe()

    button.click()
    button.click()
    button.click()
    button.click()
    button.click()

    assert(numberOfClicks === 5)
  }

}
