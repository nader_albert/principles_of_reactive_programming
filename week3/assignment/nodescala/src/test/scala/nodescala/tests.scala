package nodescala

import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be completed") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }

  test("the never combinator should return a Future that never completes") {
    val neverCombinator = Future.never[Int]

    try {
      Await.result(neverCombinator, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => assert(true)
    }
  }

  test("the never combinator should return a Future that never completes even after 20 seconds ") {
    val neverCombinator = Future.never[Int]

    try {
      Await.result(neverCombinator, 20 second)
      assert(false)
    } catch {
      case t: TimeoutException => assert(true)
    }
  }

  test("the all combinator should return a Future holding the list of values encapsulated in the list of given futures") {
    val combinedFuture = Future.all(List(
      Future{
        Thread.sleep(5000)
        1
      },
      Future{
        Thread.sleep(20)
        2
      },
      Future{15},
      Future{3})
    )

    assert(combinedFuture.isCompleted === false)
    assert(Await.result(combinedFuture,5 second) === List(1,2,15,3))
    assert(combinedFuture.isCompleted === true)
  }

  test("the all combinator should return a Future that completes with a failure if any of the given Futures fails") {
    val combinedFuture = Future.all(List(Future{1}, Future{2}, Future.failed(new RuntimeException)))

    val fallBackToFuture = combinedFuture.fallbackTo(Future{7})

    assert(combinedFuture.isCompleted)
    assert(Await.result(fallBackToFuture,1 second) === 7)
    intercept[RuntimeException](Await.result(combinedFuture,1 second))
  }

  test("the any combinator should return a Future that represents the first completed future in the given list") {
    val firstFutureToComplete = Future.any(List(Future{Thread.sleep(2);1}, Future{Thread.sleep(5); 2}, Future{Thread.sleep(10); 3}))

    assert(firstFutureToComplete.isCompleted === false)
    assert(Await.result(firstFutureToComplete,1 second) === 1)
    assert(firstFutureToComplete.isCompleted === true)
  }

  test("any combinator returns a Future representing the first future to complete in the given list, when the first one to complete completes with failure") {
    val firstFutureToComplete = Future.any(List(Future{Thread.sleep(5);1}, Future{Thread.sleep(12);2}, Future.failed(new RuntimeException)))

    assert(firstFutureToComplete.isCompleted === false)
    intercept[RuntimeException](Await.result(firstFutureToComplete,1 second))
    assert(firstFutureToComplete.isCompleted === true)
  }

  test("delay not complete"){
    val delayedFuture = Future.delay(3 second)

    println("after thread")
    intercept[TimeoutException](Await.ready(delayedFuture,2 second))
  }

  test("delay complete"){
    val delayedFuture = Future.delay(5 second)

    println("after thread")

    delayedFuture onComplete {_ => println("completed successfully !")}

    delayedFuture onSuccess {case _ => println("completed successfully !")}

    assert(Await.ready(delayedFuture,7 second).isCompleted === true)
  }

  test("now returns the value"){
    val future = Future(4)
    assert(future.now === 4)

    val future2 = Future{Thread.sleep(100); 5}
    intercept[NoSuchElementException](future2.now)
  }

  test("CancellationTokenSource should allow stopping the computation") {
    val cts = CancellationTokenSource()
    val ct = cts.cancellationToken
    val p = Promise[String]()

    async {
      while (ct.nonCancelled) {
        // do work
      }
      p.success("done")
    }

    cts.unsubscribe()
    assert(Await.result(p.future, 1 second) == "done")
  }

  test("run should return a valid subscription"){
    val working = Future.run() { ct =>
      Future {
        while (ct.nonCancelled) {
          println("working")
        }
        println("done")
      }
    }

    val delayedFuture = Future.delay(0.5 seconds)

    delayedFuture onSuccess {
      case _ => working.unsubscribe(); println("unsubscribed")
    }

    assert(Await.ready(delayedFuture,1 second).isCompleted === true)
  }

  test("continueWith_1") {
    val delay = Future.delay(1 second)
    val always = (f: Future[Unit]) => 42

    intercept[TimeoutException](Await.result(delay.continueWith(always), 500 millis))
  }

  test("continueWith_2") {
    val delay = Future.delay(1 second)
    val always = (f: Future[Unit]) => 42

    assert(Await.result(delay.continueWith(always), 1200 millis) === 42)
  }

  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }
  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




