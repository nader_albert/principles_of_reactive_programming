import java.util.concurrent.TimeoutException

import scala.language.postfixOps
import scala.io.StdIn
import scala.util._
import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

/** Contains basic data types, data structures and `Future` extensions.
 */
package object nodescala {

  /** Adds extensions methods to the `Future` companion object.
   */
  implicit class FutureCompanionOps(val f: Future.type) extends AnyVal {

    /** Returns a future that is always completed with `value`.
     */
    def always[T](value: T): Future[T] = Future {value}

    /** Returns a future that is never completed.
     *
     *  This future may be useful when testing if timeout logic works correctly.
     */
    def never[T]: Future[T] = {
      val p = Promise.apply()
      
      /** could have been written like this as well */
      //val p = Promise[T]
      p.future
    }

    /**
     * Given a list of futures `fs`, returns the future holding the list of values of all the futures from `fs`.
     *
     * The returned future is completed only once all of the futures in `fs` have been completed.
     *
     * The values in the list are in the same order as corresponding futures `fs`.
     *
     * If any of the futures `fs` fails, the resulting future also fails.
     */
    def all[T](fs: List[Future[T]]): Future[List[T]] = {
      /** ************************ Implementation # 1 ******************** */
      /*
        val mappedList: List[Future[List[T]]] = fs.map(singleFutureOfT => singleFutureOfT.map(singleT => List(singleT)))

        mappedList.foldLeft(Future {
          List[T]()
        })((futureOfListOfT1, futureOfListOfT2) => futureOfListOfT1.zip(futureOfListOfT2).map(tuple => tuple._1 ++ tuple._2))
      */
      //mappedList.foldRight(Future{List[T]()}) ((futureOfListOfT1, futureOfListOfT2) => futureOfListOfT1.zip(futureOfListOfT2).map(tuple => tuple._1 ++ tuple._2))
      /** ************************ Implementation # 2 ******************** */
      /*fs.foldRight(Future {
        List[T]()
      })((elem, acc) => elem.zip(acc).map(tuple => tuple._2.::(tuple._1)))
*/
      /** ************************ Implementation # 3 ******************** */
      /*fs.foldLeft(Future {
          List[T]()
        })((acc, elem) => elem.zip(acc).map(tuple => tuple._2.::(tuple._1)))
      */
      /** ************************ Implementation # 4 ******************** */
      fs.foldRight(
        Future {
          List[T]()
        }) ((elem, acc) =>
        for {
          value <- elem
          listOfValue <- acc
        } yield value :: listOfValue //here, i am expected to provide the internal value of the monad and the compiler will apply map on it to compose the original monad which in this case is a Future, since the accumulator is a Future monad
      )
    }

    /** Given a list of futures `fs`, returns the future holding the value of the future from `fs` that completed first.
     *  If the first completing future in `fs` fails, then the result is failed as well.
     *
     *  E.g.:
     *
     *      Future.any(List(Future { 1 }, Future { 2 }, Future { throw new Exception }))
     *
     *  may return a `Future` succeeded with `1`, `2` or failed with an `Exception`.
     */
    def any[T](fs: List[Future[T]]): Future[T] = {
      val p = Promise[T]()

      fs.foreach(_ onComplete(tryValue =>
        p.tryComplete(tryValue)
      ))

      p.future
    }

    /** Returns a future with a unit value that is completed after time `t`.
     */
    def delay(t: Duration): Future[Unit] =
      Future(
        blocking {
          println("waiting")
          Thread.sleep(t.toMillis)
          println("done waiting")
        })

    /** Completes this future with user input.
     */
    def userInput(message: String): Future[String] = Future {
      blocking {
        StdIn.readLine(message)
      }
    }

    /** Creates a cancellable context for an execution and runs it.
     */
    def run()(f: CancellationToken => Future[Unit]): Subscription = {
      val ct  = CancellationTokenSource()
      f(ct.cancellationToken)
      ct
    }
  }

  /** Adds extension methods to future objects.
   */
  implicit class FutureOps[T](val f: Future[T]) extends AnyVal {

    /** Returns the result of this future if it is completed now.
     *  Otherwise, throws a `NoSuchElementException`.
     *
     *  Note: This method does not wait for the result.
     *  It is thus non-blocking.
     *  However, it is also non-deterministic -- it may throw or return a value
     *  depending on the current state of the `Future`.
     */
    def now: T = {
      try {
        Await.result(f,0 second)
      } catch {
        case tox: TimeoutException => throw new NoSuchElementException
        case any => throw any
      }
    }

    /** Continues the computation of this future by taking the current future
     *  and mapping it into another future.
     *
     *  The function `cont` is called only after the current future completes.
     *  The resulting future contains a value returned by `cont`.
     */
    def continueWith[S](cont: Future[T] => S): Future[S] = {
      val promise = Promise[S]()

      f onComplete {
        case Success(value) => {
          try {
            val s = cont(f)
            promise.success(s)
          } catch {
            case ex: Exception => promise.failure(ex)
          }
        }
        case Failure(exception) => promise.failure(exception)
      }
      promise.future
    }

    /** Continues the computation of this future by taking the result
     *  of the current future and mapping it into another future.
     *
     *  The function `cont` is called only after the current future completes.
     *  The resulting future contains a value returned by `cont`.
     */
    def continue[S](cont: Try[T] => S): Future[S] = { //TODO: add testing
      val promise = Promise[S]()

      //f onComplete(x => promise.success(cont(x)))

      f onComplete{
        case Success(value) => {
          try {
            val s = cont(Success(value))
            promise.success(s)
          } catch {
            case ex: Exception => promise.failure(ex)
          }
        }
        case Failure(exception) => promise.failure(exception)
      }
      promise.future
    }
  }

  /**
   * Subscription objects are used to be able to unsubscribe from some event source.
   */
  trait Subscription {
    def unsubscribe(): Unit
  }

  object Subscription {
    /** Given two subscriptions `s1` and `s2` returns a new composite subscription
     *  such that when the new composite subscription cancels both `s1` and `s2`
     *  when `unsubscribe` is called.
     */
    def apply(s1: Subscription, s2: Subscription) = new Subscription {
      def unsubscribe() {
        s1.unsubscribe()
        s2.unsubscribe()
      }
    }
  }

  /** Used to check if cancellation was requested.
   */
  trait CancellationToken {
    def isCancelled: Boolean
    def nonCancelled = !isCancelled
  }

  /** The `CancellationTokenSource` is a special kind of `Subscription` that
   *  returns a `cancellationToken` which is cancelled by calling `unsubscribe`.
   *
   *  After calling `unsubscribe` once, the associated `cancellationToken` will
   *  forever remain cancelled -- its `isCancelled` will return false.
   */
  trait CancellationTokenSource extends Subscription {
    def cancellationToken: CancellationToken
  }

  /**
   * Creates cancellation token sources.
   */
  object CancellationTokenSource {

    /** Creates a new `CancellationTokenSource`.
     */
    def apply() = new CancellationTokenSource {
      val p = Promise[Unit]()

      val cancellationToken = new CancellationToken { //TODO: check why the cancellationToken was overridden as a val not def
        def isCancelled = p.future.value != None
      }

      def unsubscribe() {
        p.trySuccess(()) //upon unsubscribe, try to wrap it up with a success.
      }
    }
  }
}

