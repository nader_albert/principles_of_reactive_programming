package kvstore

import akka.actor.{Cancellable, Props, Actor, ActorRef}
import kvstore.Persistence.Persist
import scala.concurrent.duration._

object Replicator {
  // From the primary replica to the Replicator
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)

  // From the Replicator to the secondary Replica
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import Replica._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]

  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  scheduleSnapshotRetry

  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case replicate :Replicate => {
      val seq = nextSeq
      acks = acks.updated(seq, (sender,replicate))

      //println(replicate)

      //println("acks are: " + acks)
      //replica ! Snapshot(replicate.key,replicate.valueOption,seq)
    }
    case SnapshotAck(key, seq) => {
      acks.get(seq).foreach(tuple => { tuple._1 ! Replicated(tuple._2.key,tuple._2.id); println("sending replicated for: " + tuple._2.id)})
      acks = acks - seq
    }
  }

  private def scheduleSnapshotRetry: Cancellable = { //The ack list is being accessed in two different contexts !
    context.system.scheduler.schedule(0 second, 0.1 second) {
      //println("snapshot scheduler running! ")
      acks.foreach(ack => replica ! Snapshot(ack._2._2.key,ack._2._2.valueOption,ack._1))
    }
  }

}
