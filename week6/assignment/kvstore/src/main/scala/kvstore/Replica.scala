package kvstore

import akka.actor.SupervisorStrategy.{Restart, Resume, Stop}
import akka.actor._
import kvstore.Arbiter._
import scala.concurrent.duration._

import scala.util.Random
import scala.language.postfixOps

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  trait PendingAck {
    def timeOutScheduler: Cancellable

    def retryScheduler: Option[Cancellable]

    def secondaryNode: Option[ActorRef]
  }

  case class PendingPersistence(timeOutScheduler: Cancellable,
                                retryScheduler: Option[Cancellable],
                                secondaryNode: Option[ActorRef]) extends PendingAck

  case class PendingReplication(timeOutScheduler: Cancellable,
                                retryScheduler: Option[Cancellable],
                                secondaryNode: Option[ActorRef]) extends PendingAck

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {

  import Replica._
  import Replicator._
  import Persistence._
  import Math.max
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // join the team as soon as the replica is being created.
  arbiter ! Join

  //all operations are supposed to be considered failed if no valid response can be received within 1 second

  // TODO: RECEIVE TIMEOUT BEHAVIOR IS NOT SET

  var kv = Map.empty[String, String]

  var nextExpectedSeq = 0L

  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]

  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var persistAcks = Map.empty[Long,(Cancellable, ActorRef)]

  var pendingOperations = Map.empty[Long, (Option[ActorRef], Set[PendingAck])]

  //create a persistence actor
  val persistence = context.actorOf(persistenceProps)

  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  override val supervisorStrategy = OneForOneStrategy(){
    case _:PersistenceException => {
      println("persistence failure reached the Replica ! resuming persistence....")
      Stop
    }
    case _ => Stop
  }

  def receive = {
    case JoinedPrimary => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key: String, value: String, operationId: Long) => {
      kv = kv updated(key, value)
      val persistCommand = Persist(key, Some(value), operationId)

      var pendingReplicationAcks: Set[PendingReplication] = Set.empty[PendingReplication]

      val pendingPersistenceAck = PendingPersistence(scheduleTimeout(operationId,sender),
        Some(schedulePersistenceRetry(persistCommand)), None)

      secondaries.foreach{ secondary =>
        pendingReplicationAcks = pendingReplicationAcks +
          PendingReplication(scheduleTimeout(operationId, sender), None, Some(secondary._1))

        secondary._2 ! Replicate(persistCommand.key, persistCommand.valueOption, persistCommand.id)
      }

      //println("persisting operation: " + operationId)
      //persistence ! persistCommand

      pendingOperations = pendingOperations.updated(operationId, (Some(sender), (Set.empty[PendingAck] + pendingPersistenceAck)
        ++ pendingReplicationAcks))
    }

    case Remove(key: String, operationId: Long) => {
      kv = kv - key
      val persistCommand = Persist(key, None, operationId)

      var pendingReplicationAcks: Set[PendingReplication] = Set.empty[PendingReplication]

      secondaries.foreach { secondary =>
        //secondary._2 ! Replicate(persistCommand.key, persistCommand.valueOption, persistCommand.id)
        pendingReplicationAcks = pendingReplicationAcks +
          PendingReplication(scheduleTimeout(operationId, sender), None, Some(secondary._1))

        secondary._2 ! Replicate(persistCommand.key, persistCommand.valueOption, persistCommand.id)
      }

      //println("persisting operation: " + operationId)
      //persistence ! persistCommand

      val pendingPersistenceAck = PendingPersistence(scheduleTimeout(operationId, sender),
        Some(schedulePersistenceRetry(persistCommand)), None)

      pendingOperations = pendingOperations.updated(operationId,
        (Some(sender), (Set.empty[PendingAck] + pendingPersistenceAck) ++ pendingReplicationAcks))
    }

    case Get(key: String, id: Long) => sender ! GetResult(key, kv.get(key), id)

    case Replicas(replicas) => {
      val comparisonOutcome = compareReplicas(replicas - self, secondaries.keys.toSet)
      var operationId = 1000L //TODO: RESOLVE THE PROBLEM OF THE ID's HERE!

      comparisonOutcome._1.foreach(
        joiningNode => {
          val replicator = context.actorOf(Replicator.props(joiningNode), "replicator-" + Random.nextInt())

          secondaries = secondaries + ((joiningNode , replicator))

          kv.foreach{ storeEntry =>
            pendingOperations = pendingOperations.updated(operationId,
              (None, Set.empty[PendingAck] + PendingReplication(scheduleTimeout(operationId, sender), None, Some(joiningNode))))

            replicator ! Replicate(storeEntry._1, Some(storeEntry._2), operationId)

            operationId = operationId + 1
          }
        }
      )

      comparisonOutcome._2.foreach(
        removedNode => {
          secondaries.get(removedNode).foreach(_ ! PoisonPill)
          secondaries = secondaries - removedNode
          pendingOperations = pendingOperations.map{
            pendingOperation => {
              pendingOperation._2._2.find(ack =>
                ack.secondaryNode.isDefined && ack.secondaryNode.get.equals(removedNode)).map { pendingAck => {
                  (pendingOperation._1, (pendingOperation._2._1, pendingOperation._2._2 - pendingAck))
                }
              }.getOrElse(pendingOperation)
            }
          }
          pendingOperations = updateOperationAcknowledgmentQueue
        }
      )
    }

    case Persisted(_,id) => { println("operation: " + id + " persisted successfully!"); acknowledgePersistence(id) }

    case Replicated(_,id) => { println("operation: " + id + " replicated successfully!");  acknowledgeReplication(id, sender) }
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    case Get(key: String, id: Long) => sender ! GetResult(key, kv.get(key), id)

    case Snapshot(key, value, seq) if seq < nextExpectedSeq => sender ! SnapshotAck(key, seq)

    case Snapshot(key, value, seq) if seq > nextExpectedSeq => //ignore

    case Snapshot(key, value, seq) => {
      value match {
        case Some(v) => {
          kv = kv.updated(key, v)
          val persistCommand = Persist(key, Some(v), seq)
          //persistence ! persistCommand
          val scheduler = schedulePersistenceRetry(persistCommand)
          persistAcks = persistAcks.updated(seq,(scheduler,sender))
        }
        case None => {
          kv = kv - key
          val persistCommand = Persist(key, None, seq)
          //persistence ! persistCommand
          val scheduler = schedulePersistenceRetry(persistCommand)
          persistAcks = persistAcks.updated(seq,(scheduler,sender))
        }
      }
    }

    case Persisted(key, id) => {
      acknowledgeSecondary(key,id)
      nextExpectedSeq = max(nextExpectedSeq, id + 1)
    }
  }

  private def compareReplicas(newReplicas: Set[ActorRef], storedReplicas: Set[ActorRef]): (Set[ActorRef], Set[ActorRef])= {
    val unChangedNodes = newReplicas.filter(storedReplicas.contains)

    val nodesJustJoined = newReplicas -- unChangedNodes
    val nodesJustRemoved = storedReplicas -- unChangedNodes

    (nodesJustJoined,nodesJustRemoved)
  }

  private def schedulePersistenceRetry(persistCommand: Persist): Cancellable =
    context.system.scheduler.schedule(0 second, 0.1 second) {
      println("retrying persistence for operation: " + persistCommand.id + "..... ")
      persistence ! persistCommand
    }

  private def scheduleTimeout(operationId: Long, receiver: ActorRef): Cancellable = {
    println("schduling timeout for operation: " + operationId)
    context.system.scheduler.scheduleOnce(1 second) {
      println("operation: " + operationId + " has timed out ....")

      pendingOperations.get(operationId).foreach(pendingOp => pendingOp._2.foreach(_.retryScheduler.foreach(_.cancel)))
      pendingOperations = pendingOperations - operationId

      receiver ! OperationFailed(operationId)
    }
  }

  private def acknowledgePersistence(operationId: Long) = {
    assert (!pendingOperations.get(operationId).isEmpty, "trying to acknowledge persistence for: " + operationId + " again !")

    val operationPendingAcks = pendingOperations.get(operationId).get

    pendingOperations = pendingOperations.updated(operationId, (operationPendingAcks._1,
      operationPendingAcks._2.flatMap(pendingAck =>
        pendingAck match {
          case PendingPersistence(timeOutScheduler, retryScheduler, _) => {
            println("persistence acknowledge received... cancelling timeout and retry schedulers .... for operation" + operationId)

            retryScheduler.foreach(_.cancel)
            timeOutScheduler.cancel
            Set.empty[PendingAck]
          }
          case _ => Set.empty[PendingAck] + pendingAck
        })))

    updateOperationAcknowledgmentQueue(operationId)
  }

  /**
   * if the operationId can't be found in the map, then the logic herein won't execute.. and this is required to ensure
   * that only one acknowledgment will be sent to the caller, even if more than Persisted messages is being received !
   */
  private def acknowledgeReplication(operationId: Long, acknowledgingNode: ActorRef) = {
    assert (!pendingOperations.get(operationId).isEmpty, "trying to acknowledge replication for: " + operationId + " again !")

    val operationPendingAcks = pendingOperations.get(operationId).get

    pendingOperations = pendingOperations.updated(operationId, (operationPendingAcks._1,
      operationPendingAcks._2.flatMap(pendingAck =>
        pendingAck match {
          case PendingReplication(timeOutScheduler, retryScheduler, secondaryNode)
            if secondaryNode.fold(false)(secondaries.get(_).fold(false)(_.equals(acknowledgingNode))) => {
            timeOutScheduler.cancel
            retryScheduler.foreach(_.cancel)
            Set.empty[PendingAck]
          }
          case _ => Set.empty[PendingAck] + pendingAck
        }
      )))
    updateOperationAcknowledgmentQueue(operationId)
  }

  private def acknowledgeSecondary(key: String, replicationSeq: Long) = {
    persistAcks.get(replicationSeq).foreach(
      tuple => {
        tuple._1.cancel
        tuple._2 ! SnapshotAck(key, replicationSeq)
      }
    )
    persistAcks = persistAcks - replicationSeq
  }

  private def updateOperationAcknowledgmentQueue(operationId: Long) =
    pendingOperations.get(operationId).foreach(pendingOperation => {
      if (pendingOperation._2.isEmpty) {
        pendingOperations = pendingOperations - operationId
        pendingOperation._1.foreach(_ ! OperationAck(operationId))
      }})

  private def updateOperationAcknowledgmentQueue() =
    pendingOperations.filterNot(pendingOperation => {
      if (pendingOperation._2._2.isEmpty) {
        pendingOperation._2._1.foreach(_ ! OperationAck(pendingOperation._1))
        true
      }
      else false
    })
}

//It is the job of the Replica actor to create and appropriately supervise the Persistence actor; for the purpose of
//this exercise any strategy will work, which means that you can experiment with different designs based on resuming,
//restarting or stopping and recreating the Persistence actor. To this end your Replica does not receive an ActorRef but
//a Props for this actor, implying that the Replica has to initially create it as well.
//For grading purposes it is expected that Persist is retried before the 1 second response timeout in case persistence failed.
//The id used in retried Persist messages must match the one which was used in the first request for this particular update.

//You should write (versions of) tests which exercise the behavior under unreliable persistence
//(i.e. when using a Persistence actor created with flaky = true) or unreliable communication between primary and
//secondaries. The latter can be done by having the Arbiter wrap the secondary nodes’ ActorRefs in small actors which
//normally forward messages but sometimes forget to do so.

