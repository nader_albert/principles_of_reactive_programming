package calculator

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest._

import TweetLength.MaxTweetLength

import scala.collection.immutable.HashMap

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers {

  /******************
   ** TWEET LENGTH **
   ******************/

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }

  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  test("test direct cycling"){
    val cells = HashMap(
      "a" -> Signal[Expr](Plus(Ref("b"), Literal(1))),
      "b" -> Signal[Expr](Ref("a")))

    assert(Calculator.computeValues(cells).values.map(_()).sum.toString == "NaN")
  }

  test("test indirect cycling"){
    val cells = HashMap(
      "a" -> Signal[Expr](Plus(Ref("b"), Literal(1))),
      "b" -> Signal[Expr](Ref("c")),
      "c" -> Signal[Expr](Ref("d")),
      "d" -> Signal[Expr](Ref("b"))
    )

    assert(Calculator.computeValues(cells).values.map(_()).sum.toString == "NaN")
  }

  test("test indirect cycling 2"){
    val cells = HashMap(
      "a" -> Signal[Expr](Plus(Ref("b"), Literal(1))),
      "b" -> Signal[Expr](Times(Ref("c"), Ref("a"))),
      "c" -> Signal[Expr](Ref("d")),
      "d" -> Signal[Expr](Literal(5))
    )

    assert(Calculator.computeValues(cells).values.map(_()).sum.toString == "NaN")

    assert(Calculator.computeValues(cells).values.map(_()).head.toString == "NaN")
    assert(Calculator.computeValues(cells).values.map(_()).tail.head.toString == "NaN")

    assert(Calculator.computeValues(cells).values.map(_()).init.last == 5.0)
    assert(Calculator.computeValues(cells).values.map(_()).last == 5.0)
  }

  test("test no cycling"){
    val cells = HashMap(
      "a" -> Signal[Expr](Plus(Ref("b"), Literal(1))),
      "b" -> Signal[Expr](Ref("c")),
      "c" -> Signal[Expr](Ref("d")),
      "d" -> Signal[Expr](Literal(5))
    )

    assert(Calculator.computeValues(cells).values.map(_()).sum == 21.0)
  }

  /*test("second degree polynomial") {
    assert(Polynomial.computeSolutions(Signal(-1), Signal(-1), Signal (-1), Signal(-3))().toString == (Set(Double.NaN) + Double.NaN).toString)
  }

  test("second degree polynomial with -1's") {
    assert(Polynomial.computeSolutions(Signal(-1), Signal(-1), Signal (-1), Signal(-3))().toString == (Set(Double.NaN) + Double.NaN).toString)
  }

  test("second degree polynomial with zero's ") {
    assert(Polynomial.computeSolutions(Signal(0), Signal(0), Signal (0), Signal(0))().toString == (Set(Double.NaN) + Double.NaN).toString)
  }

  test("second degree polynomial with zero and incorrect delta ") {
    assert(Polynomial.computeSolutions(Signal(0), Signal(0), Signal (0), Signal(2))().toString == (Set(Double.NaN) + Double.NaN).toString)
  }*/

}
