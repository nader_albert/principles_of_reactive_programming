package calculator

import math.sqrt

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double], c: Signal[Double]): Signal[Double] = {
    Signal{
      val bValue = b()
      val aValue = a()
      val cValue = c()
      bValue * bValue - (4 * aValue * cValue)
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double], c: Signal[Double], delta: Signal[Double])
  : Signal[Set[Double]] = {
      Signal{
        val bValue = b()
        val aValue = a()
        //val cValue = c()

        //val delta1 = computeDelta(a,b,c)()
        //val delta2 = computeDelta(a,b,c)()

        if (delta() < 0)
          Set()
        else
          Set(- (bValue + sqrt(delta())) / (2 * aValue)) + (- (bValue - sqrt(delta())) / (2 * aValue) )
      }
    }
}