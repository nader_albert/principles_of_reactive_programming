package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {
    //this: IntHeap =>

    property("inserting an integer value into an empty heap, should be the minimum value of the resulting heap") =
      forAll { a: Int =>
        findMin(insert(a, empty)) == a
    }

    property("finding the minimum of the heap resulting from inserting two integer values in an empty heap, " +
      "should be the smaller value out of those two") = forAll { (a1: Int, a2: Int) =>
        findMin(insert(a2, insert(a1, empty))) == ord.min(a1,a2)
    }

    property("deleting the minimum of a single item heap, should result in an empty heap") = forAll { a: Int =>
      isEmpty(deleteMin(insert(a, empty)))
    }

    property("the minimum value of a heap resulting from melding two other heaps, should be equal to the minimum value " +
      "of one of the two heaps ") = forAll { (a1: Int, a2:Int) =>

        val h1 = insert(a1,empty)
        val h2 = insert(a2,empty)

        val h3 = meld(h1,h2)
        val min = findMin(h3)

        min == findMin(h1) || min == findMin(h2)
    }

    property("sorted") = forAll { (a1: Int, a2:Int, a3:Int) =>
      val h1 = insert(a1,empty)
      val h2 = insert(a2,h1)
      val h3 = insert(a3,h2)

      orderedSeq(h3).sorted == orderedSeq(h3)
    }

    def orderedSeq(h:H) : List[A] = isEmpty(h) match {
      case true => Nil
      case false => findMin(h)::orderedSeq(deleteMin(h))
    }

    // ******* heap generator based properties **********
    property("the minimum value of a heap should remain unchanged if the minimum value of a non-empty heap is re-inserted" )
      = forAll { (h: H) =>
        if (!isEmpty(h)) {
          val m = if (isEmpty(h)) 0 else findMin(h)
          findMin(insert(m, h)) == m
        } else
          true
    }

    property("the minimum value of the heap resulting from melding a non empty heap with an empty heap is equal to " +
      "the minimum of the non-empty heap") = forAll { (h: H) =>
        if(!isEmpty(h))
          findMin(meld(h,empty)) == findMin(h)
        else
          true
    }

    property("finding the minimum of the heap resulting from the meld of a non-empty heap with itself, should be equal " +
      "to the minimum value of the non-empty heap itself") = forAll { (h: H) =>
        isEmpty(h) match {
          case false => findMin(meld(h, h)) == findMin(h)
          case _ => true
        }
    }

    property("gen2") = forAll { (h1: H, h2 : H) =>
      if(!isEmpty(h1) && !isEmpty(h2)) {
        val minimumOfMeld = findMin(meld(h1,h2))
        minimumOfMeld == findMin(h1) || minimumOfMeld == findMin(h2)
      }
      else
        true
    }

    property("gen3") = forAll { (h1: H, h2 : H) =>
      if(!isEmpty(h1) && !isEmpty(h2)) {
        val minimumOfDeleteAfterMeld = findMin(deleteMin(meld(h1,h2)))

        minimumOfDeleteAfterMeld == findMin(deleteMin(h1)) ||
          minimumOfDeleteAfterMeld == findMin(deleteMin(h2)) ||
          minimumOfDeleteAfterMeld == findMin(h1) ||
          minimumOfDeleteAfterMeld == findMin(h2)
      }
      else
        true
    }

    property("gen4") = forAll { (h1: H, a: Int) =>
      if(!isEmpty(h1)) {
        if (a < findMin(h1))
          findMin(insert(a,h1)) == a
        else
          findMin(insert(a,h1)) == findMin(h1)
      }
      else
        true
    }

    property("gen3") = forAll { (h1: H, h2: H, a:Int) =>
      if(!isEmpty(h1) && !isEmpty(h2)) {
        val minimumOfMeldInsert = findMin(meld(insert(a,h1),insert(a,h2)))
        minimumOfMeldInsert == findMin(h1) || minimumOfMeldInsert == findMin(h2) || minimumOfMeldInsert == a
      }
      else
        true
    }

    property("the size of the heap resulting from melding two non-empty heaps should equal to the size of the first + " +
      "the size of the second  ") = forAll { (h1: H, h2: H) =>
        if(!isEmpty(h1) && !isEmpty(h2))
          orderedSeq(meld(h1,h2)).size == orderedSeq(h1).size + orderedSeq(h2).size
        else
          true
    }

    property("getting the summation of all the elements of heap resulting from the meld of two non-empty heaps, should" +
      " be equal to the summation of the elements of the first heap + those of the second heap ") =
      forAll { (h1: H, h2: H) =>
        if(!isEmpty(h1) && !isEmpty(h2))
          orderedSeq(meld(h1,h2)).sum == orderedSeq(h1).sum + orderedSeq(h2).sum
        else
          true
    }

    property("inserting an integer value into the heap resulting from the meld of two non-empty heaps, should compose a " +
      "heap  that is the same as the heap resulting from the heap having the same integer value melded with the same " +
      "non-empty heaps  ") =
      forAll { (h1: H, h2: H, a: Int) =>
        insert(a,meld(h1,h2)) == meld(insert(a,empty), meld(h1,h2))
    }

    property("the minimum value of the heap resulting from melding a heap with itself, is equal to the minimum value of " +
      "this heap ") = forAll { (h1: H) =>
        findMin(meld(h1, h1)) == findMin(h1)
    }

    property("the minimum value of the heap resulting from melding two other heaps, should be equal to the minimum of " +
      "one of the two heaps ") = forAll { (h1: H, h2: H) =>
        findMin(meld(h1,h2)) == findMin(h1) || findMin(meld(h1,h2)) == findMin(h2)
    }

    property("gen3") = forAll { (h: H, a: Int) =>
      if(!isEmpty(h)) {
        //gamda ta7n bardo bel 3end ba2a
        val min = findMin(deleteMin(meld(h, insert(a, empty))))

        min == findMin(h) || min == findMin(deleteMin(h)) || min == a
      }
      else
        true
    }

    property("iteratively finding the minimum of a heap and deleting it, should result in an ordered list of values") =
      forAll { (h: H) =>
        isEmpty(h) match {
          case false => orderedSeq (h).sorted == orderedSeq (h)
          case _ => true
        }
    }

    property("minimum of a heap should always be less than the minimum of the same heap after deleting its minimum value ") =
      forAll { (h: H) =>
        if(!isEmpty(h) && ! isEmpty(deleteMin(h)))
          findMin(h) <= findMin(deleteMin(h))
        else
          true
    }

    property("inserting the minimum value of a heap after deleting it, should result in a heap, whose minimum value is " +
      "the original minimum value ") = forAll { (h: H) =>
        isEmpty(h) match {
          case false => {
            val m = findMin(h)
            findMin(insert(m, deleteMin(h))) == m
          }
          case _ => true
        }
    }

    property("the minimum value of a heap, should be the same after deleting the minimum value from a heap and then " +
      "melding it with its original version ") = forAll { (h: H) =>
      isEmpty(h) match {
        case false => findMin (meld (deleteMin (h), h) ) == findMin (h)
        case _ => true
      }
    }

    property("the minimum value of the heap resulting from melding a heap with itself after deleting its minimum value " +
      "from it, should be the same as the minimum value found in the same heap after deleting the minimum value from it") =
      forAll {
        (h: H) => {
          isEmpty(h) match {
            case true => true //isEmpty(deleteMin(h)) // must verify that an exception is thrown
            case false => val h2 = deleteMin(h)
              isEmpty(h2) match {
                case false => findMin(meld(deleteMin(h),deleteMin(h))) == findMin(deleteMin(h))
                case _ => true
              }
          }
        }
    }

    property("the minimum value of a heap after removing its original minimum value, should be less than or equal to " +
      "the minimum value that was deleted ") = forAll {
        (h: H) =>
          isEmpty(h) || isEmpty(deleteMin(h)) match {
            case true => true
            case false => ord.gteq(findMin(deleteMin(h)), findMin(h))
          }
    }

    property("the largest value of an ordered heap after deleting the minimum value from it, should be greater than " +
      "or equal the largest value of the original heap") = forAll { (h: H) =>
        if(!isEmpty(h) && isEmpty(deleteMin(h)))
          ord.gteq(orderedSeq(deleteMin(h)).reverse.headOption.getOrElse(0), orderedSeq(h).reverse.headOption.getOrElse(0))
        else
          true
    }

    property("the minimum value of an ordered heap after deleting the minimum value from it, should be less than" +
      " or equal the largest value of the original heap") = forAll { (h: H) =>
      if(!isEmpty(h) && !isEmpty(deleteMin(h)))
        ord.gteq(orderedSeq(deleteMin(h)).headOption.getOrElse(0), orderedSeq(h).headOption.getOrElse(0))
      else
        true
    }

    property("the summation of all the values in a heap after deleting the minimum from it, should be the same as the " +
      "summation of all values in the original heap after subtracting the minimum value from it ") = forAll { (h: H) =>
        if(!isEmpty(h) && !isEmpty(deleteMin(h)))
          orderedSeq(deleteMin(h)).sum == orderedSeq(h).sum - findMin(h)
        else
          true
    }

    lazy val genHeap: Gen[H] =
      for {
        a <- arbitrary[Int]
        b <- arbitrary[Int]
        c <- arbitrary[Int]
        d <- arbitrary[Int]

        e <- arbitrary[Int]
        f <- arbitrary[Int]

        g <- arbitrary[Int]

        z1 <- oneOf(const(insert(e,empty))/*genHeap*/, insert(a,empty), insert (b, empty))
        z2 <- oneOf(const(insert(f,empty)),insert(c, empty))
        z3 <- const(insert (d,empty))
        z4 <- const(insert (g,empty))

        m1 <- oneOf(const(empty), genHeap)
      } yield insert(c,insert(b,insert(a,m1))) //yield meld (m1,m2) //yield insert(a,m) //yield meld(z4,meld(meld(z1, z2),z3))

    implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)
}